package com.tuxflux.p2p;

import other.Save;
import other.TCPHandlerAndroid;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Main Activity of the project. This class is used to load
 * the latest conversations as a listview.
 * 
 * @author Simon Cedergren AB1539 <simon@tuxflux.se>
 *
 */
public class MainActivity extends Activity {

	public static final String PREFS_NAME = "Settings";
	private static final String CONTACT = "contact";
	private static final String MESSAGE = "message";
	public static String NAME;
	public static String EMAIL;
	private boolean firstRun;
	private String username;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		checkAccount();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle presses on the action bar items
		switch (item.getItemId()) {
		case R.id.action_AddContact:
			addContact();
			return true;
		case R.id.action_NewChat:
			Intent contacts = new Intent(this, ContactlistActivity.class);
			startActivity(contacts);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * Checks if the user has an account or not.
	 */
	private void checkAccount() {
		SharedPreferences sp = getApplicationContext().getSharedPreferences(PREFS_NAME, 0);
		firstRun = sp.getBoolean("firstRun", true);
		if (firstRun == true) {
			makeAccount(); // Create an account
		} else {
			username = sp.getString("username", null);
			Toast.makeText(this, "Inloggad som " + username, Toast.LENGTH_LONG).show();
		}
	}

	// I am aware that I should implement a better (more secure) way of storing
	// credentials. This poor implementation is due to
	// the current (limited) time frame. This is why users only are asked to
	// submit an alias, and nothing else.
	/**
	 * Method that saves the username that the user choose.
	 * @param username - username to be saved.
	 */
	public void saveUserName(String username) {
		TCPHandlerAndroid tcp = new TCPHandlerAndroid();
		SharedPreferences sp = getApplicationContext().getSharedPreferences(PREFS_NAME, 0);
		Editor edit = sp.edit();
		if (isValidChar(username) && !username.contains(" ")) {
			//			Toast.makeText(this, getString(R.string.invalid_name), Toast.LENGTH_SHORT).show();
			edit.putString("username", username);
			edit.putBoolean("firstRun", false);
			edit.commit();
			tcp.execute(username);
			Toast.makeText(getApplicationContext(), getString(R.string.saving_name) + " \"" + username + "\"", Toast.LENGTH_LONG).show();
		} else {
			makeAccount();
		}
	}

	/**
	 * A dialog with input, used to create a new account on first run.
	 */
	private void makeAccount() {
		final EditText username = new EditText(this);
		AlertDialog.Builder ad = new AlertDialog.Builder(this);
		ad.setCancelable(false); // Makes sure that the user does not close the dialog by tapping outside of it.
		ad.setTitle(R.string.create_account_title);
		ad.setMessage(R.string.create_account_message);
		ad.setView(username);
		ad.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				saveUserName(username.getText().toString());
			}
		});
		ad.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				Toast.makeText(getApplicationContext(), R.string.app_killed, Toast.LENGTH_LONG).show();
				makeAccount();
			}
		});
		ad.show();
	}

	/**
	 * Writes new contact to contact-file. 
	 * TODO: Check db if contact really exists.
	 */
	private void addContact() {
		final EditText newcontact = new EditText(this);
		AlertDialog.Builder ad = new AlertDialog.Builder(this);
		ad.setTitle(getString(R.string.add_contact));
		ad.setView(newcontact);
		ad.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				if (isValidChar(newcontact.getText().toString())) {
					Save save = new Save(getApplicationContext());
					save.add(new String[]{CONTACT, newcontact.getText().toString()});
					Toast.makeText(getApplicationContext(), "Added contact: " + newcontact.getText().toString(), Toast.LENGTH_SHORT).show();
				} else {
					Toast.makeText(getApplicationContext(), getString(R.string.user_does_not_exist) + " \"" + newcontact.getText().toString() + "\"", Toast.LENGTH_SHORT).show();
				}
			}
		});
		ad.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				dialog.dismiss();
			}
		});
		ad.show();
	}
	
	/**
	 * Method to check if input is valid or not.
	 * 
	 * Coutesy of:
	 * http://stackoverflow.com/questions/749742/java-make-sure-a-string
	 * -contains-only-alphanumeric-spaces-and-dashes
	 * 
	 * @param seq - characters to be checked.
	 * @return true if all characters is valid.
	 */
	private boolean isValidChar(CharSequence seq) {
		int len = seq.length();
		for (int i = 0; i < len; i++) {
			char c = seq.charAt(i);
			// Test for all positive cases
			if ('0' <= c && c <= '9')
				continue;
			if ('a' <= c && c <= 'z')
				continue;
			if ('A' <= c && c <= 'Z')
				continue;
			if (c == '-')
				continue;
			// ... insert more positive character tests here
			// If we get here, we had an invalid char, fail right away
			Toast.makeText(this, getString(R.string.invalid_char) + " \"" + seq.charAt(i) +"\"", Toast.LENGTH_LONG).show();
			return false;
		}
		// All seen chars were valid, succeed
		return true;
	}
}